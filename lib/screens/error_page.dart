import 'package:flutter/material.dart';
import 'package:flutter_base/app_localization.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              AppLocalizations.of(context).translate("not_found_code"),
              style: const TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
            ),
            Text(
              AppLocalizations.of(context).translate("not_found_message"),
              style: const TextStyle(fontSize: 18),
            )
          ],
        ),
      ),
    );
  }
}
