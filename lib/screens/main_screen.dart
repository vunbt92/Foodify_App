import 'package:flutter/material.dart';
import 'package:flutter_base/screens/splash/splash_screen.dart';

class MainScreen extends StatelessWidget {
  // Parameter
  final String parameter;
  const MainScreen({Key? key, required this.parameter}) : super(key: key);

  // Route name
  static const routeName = '/home';

  @override
  Widget build(BuildContext context) {
    return SplashScreen(parameter: parameter,);
  }
}
