import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_base/alert/alert_popup.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/button.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FeedbackScreen extends StatefulWidget {
  const FeedbackScreen({Key? key}) : super(key: key);

  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: CommonColors.black11,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildBanner(context),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 30, bottom: 20),
                  child: Text(
                    'Leave us a feedback!!',
                    style: CommonStyles.w500Size24WhiteFF(context),
                  ),
                ),
                _buildTextFieldName(context),
                _buildRating(context),
                _buildTextFieldContent(context),
                Padding(
                    padding: const EdgeInsets.only(
                        left: 20, right: 20, top: 80, bottom: 50),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          AlertUtils.feedbackDialog(context);
                        });
                      },
                      child:
                          Buttons.displayButton(context, 'Submit Feedback', 10),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBanner(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.3,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              Resources.imgBgrFeedback,
              fit: BoxFit.cover,
            ),
            Positioned(
                top: 50,
                left: 20,
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(Resources.icBack)))
          ],
        ));
  }

  Widget _buildTextFieldName(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        decoration: InputDecoration(
            hintText: 'Name',
            hintStyle: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 18,
                color: CommonColors.black00.withOpacity(0.4)),
            border: InputBorder.none,
            fillColor: CommonColors.whiteFF,
            filled: true),
      ),
    );
  }

  Widget _buildRating(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          'Rating',
          style: CommonStyles.w500Size24WhiteFF(context),
        ),
        const SizedBox(height: 40),
        RatingBar(
          initialRating: 3,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          ratingWidget: RatingWidget(
            full: SvgPicture.asset(Resources.icRatingStart1),
            half: SvgPicture.asset(Resources.icRatingStart2),
            empty: SvgPicture.asset(Resources.icRatingStart3),
          ),
          itemPadding: const EdgeInsets.symmetric(horizontal: 5.0),
          onRatingUpdate: (rating) {
            log(rating.toString());
          },
        )
      ]),
    );
  }

  Widget _buildTextFieldContent(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: TextField(
          maxLines: 5,
          decoration: InputDecoration(
            hintText: "Feedback",
            hintStyle: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 18,
                color: CommonColors.black00.withOpacity(0.4)),
            border: InputBorder.none,
            fillColor: CommonColors.whiteFF,
            filled: true,
          ),
        ));
  }

  // _showDialog(BuildContext context) {
  //   return AlertDialog(
  //     title: const Text('Feedback Success'),
  //     content: const Text('Back to Home?'),
  //     actions: <Widget>[
  //       TextButton(
  //         onPressed: () => Navigator.pop(context, 'Cancel'),
  //         child: const Text('Cancel'),
  //       ),
  //       TextButton(
  //         onPressed: () => Navigator.pop(context, 'Ok'),
  //         // Navigator.push(context, MaterialPageRoute(builder: (context) => )),
  //         child: const Text('OK'),
  //       ),
  //     ],
  //   );
  // }
}
