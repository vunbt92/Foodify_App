import 'dart:developer';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/banner_screens.dart';
import 'package:flutter_base/common/ui/cards.dart';
import 'package:flutter_base/common/ui/load_more_food.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_drink.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class DesertScreen extends StatefulWidget {
  final Data? data;
  final List<MoreItemModel> listMoreFoodDrink;

  const DesertScreen(this.data, this.listMoreFoodDrink, {Key? key})
      : super(key: key);

  @override
  _DesertScreenState createState() => _DesertScreenState();
}

class _DesertScreenState extends State<DesertScreen> {
  final List<Item> _listItem = [];
  String? _keySearch;
  final List<Item> _listItemSearch = [];

  List<Item> _handleInitListItem() {
    for (var element in widget.data!.items!) {
      _listItem.clear();
      _listItem.addAll(element.item!);
    }
    return _listItem;
  }

  _handleGetListItem() {
    if (_keySearch!.isEmpty) {
      _listItem.clear();
      _handleInitListItem();
    } else {
      _listItemSearch.addAll(_listItem.where((element) {
        return element.nameFood!
            .toLowerCase()
            .contains(_keySearch!.toLowerCase());
      }).toList());
      if (_listItemSearch.isEmpty) {
        setState(() {
          _listItem.clear();
        });
        return;
      } else {
        _listItem.clear();
        setState(() {
          _listItem.addAll(_listItemSearch);
        });
        _listItemSearch.clear();
        return;
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BillBloc, BillState>(
        builder: (context, state) => _buildUI(context),
        listener: (context, state) {
          if (state is SearchLoading) {
            log('Loading...');
          } else if (state is SearchLoaded) {
            _keySearch = state.dataSearch;
            _handleGetListItem();
          } else if (state is SearchLoadError) {
            log('AAA:${state.message}');
          }
        });
  }

  Widget _buildUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          BannerScreens(context, widget.data!),
          _buildListFood(context),
          LoadMoreFood(context, widget.listMoreFoodDrink)
        ],
      ),
    );
  }

  Widget _buildListFood(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: _handleInitListItem().map((item) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => DetailItemDrink(item, _listItem)));
            },
            child: _buildItemListFood(context, item),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildItemListFood(BuildContext context, Item item) {
    final formatCurrency = NumberFormat.decimalPattern('vi');
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      color: CommonColors.grey89.withOpacity(0.2),
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      child: Stack(
        children: [
          Image.asset(item.pathImage!),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Cards.displayCardDiscount(context, item.discount!),
          ),
          Positioned(
              bottom: 10,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                    alignment: Alignment.center,
                    color: CommonColors.whiteFF.withOpacity(0.5),
                    width: MediaQuery.of(context).size.width * 0.8,
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(item.nameFood!,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: CommonStyles.w400Size18Black00(context)),
                        Flexible(
                            child: DottedLine(
                          direction: Axis.horizontal,
                          lineLength: double.infinity,
                          lineThickness: 1.0,
                          dashLength: 4.0,
                          dashColor: CommonColors.black00,
                          dashRadius: 0.0,
                          dashGapLength: 4.0,
                          dashGapRadius: 0.0,
                        )),
                        Text('${formatCurrency.format(item.price)}VND',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.end,
                            style: CommonStyles.w500Size22Black00(context)),
                      ],
                    )),
              ))
        ],
      ),
    );
  }
}
