import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/common/ui/banner_screens.dart';
import 'package:flutter_base/common/ui/load_more_food.dart';
import 'package:flutter_base/common/ui/ui_item_list.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common/common_styles.dart';

class KitchenScreen extends StatefulWidget {
  final Data? data;
  final List<MoreItemModel> listMoreFoodDrink;

  const KitchenScreen(this.data, this.listMoreFoodDrink, {Key? key})
      : super(key: key);

  @override
  _KitchenScreenState createState() => _KitchenScreenState();
}

class _KitchenScreenState extends State<KitchenScreen> {
  // final List<Items> _listItems = [];
  String? _keySearch;
  final List<Items> _listItemSearch = [];
  //
  // List<Items> _handleInitListItem() {
  //   _listItems.clear();
  //   _listItems.addAll(widget.data!.items!);
  //   return _listItems;
  // }

  _handleGetListItem() {
    _listItemSearch.clear();
    _listItemSearch.addAll(widget.data!.items!.where((element) {
      return element.keyItem!.contains(_keySearch!.toLowerCase());
    }).toList());
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BillBloc, BillState>(
        builder: (context, state) => _buildUI(context),
        listener: (context, state) {
          if (state is SearchLoading) {
            log('Loading...');
          } else if (state is SearchLoaded) {
          } else if (state is SearchLoadError) {
            log('AAA:${state.message}');
          }
        });
  }

  Widget _buildUI(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        if (widget.data != null) BannerScreens(context, widget.data!),
        _filterList(context),
        Column(
          children: _keySearch == null
              ? widget.data!.items!.map((item) {
                  return UiItemList(context, item, 'food');
                }).toList()
              : _listItemSearch.map((item) {
                  return UiItemList(context, item, 'food');
                }).toList(),
        ),
        LoadMoreFood(context, widget.listMoreFoodDrink)
      ],
    ));
  }

  // List Filter
  Widget _filterList(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          DropdownButton(
              value: _keySearch,
              style: CommonStyles.w400Size16OrangeF2(context),
              items: widget.data?.items?.map(
                (val) {
                  return DropdownMenuItem<String>(
                    value: val.keyItem,
                    child: Text(val.keyItem.toString()),
                  );
                },
              ).toList(),
              onChanged: (String? val) {
                setState(() {
                  _keySearch = val;
                  _handleGetListItem();
                });
              }),
          IconButton(
              onPressed: () {
                setState(() {
                  _keySearch = null;
                });
              },
              icon: const Icon(
                Icons.list,
                color: Colors.orange,
                size: 45,
              )),
        ],
      ),
    );
  }
}
