import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<Data> listData = [];
  List<Items> listItems = [];
  List<Item> fullData = [];
  List<Item> searchData = [];
  TextEditingController textEditingController = TextEditingController();

  //
  // _requestSearchItemBill(BuildContext context) {
  //   BlocProvider.of<BillBloc>(context).add(const RequestSearchItem());
  // }

  @override
  initState() {
    // _requestSearchItemBill(context);
    super.initState();
  }

  // search data
  onSearchTextChanged(String text) async {
    searchData.clear();
    if (text.isEmpty) {
      // Check text field is empty or not
      setState(() {});
      return;
    }
    for (var data in fullData) {
      if (data.nameFood
          .toString()
          .toLowerCase()
          .contains(text.toLowerCase().toString())) {
        searchData.add(
            data); // If not empty then add search data into search data list
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BillBloc, BillState>(
        builder: (context, state) => _buildUI(context),
        listener: (context, state) {
          if (state is SearchLoading) {
          }
          // else if (state is ItemSearchLoaded) {
          //   listData = state.foodSearch!.data!;
          //   for (var item in listData) {
          //     listItems.addAll(item.items!);
          //   }
          //   for (var element in listItems) {
          //     fullData.addAll(element.item!);
          //   }
          // }
          else if (state is SearchLoadError) {
            log('error:${state.message}');
          }
        });
  }

  Widget _buildUI(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            _searchField(context),
            _resultSearch(context),
          ],
        ),
      ),
    );
  }

  // Search Form Field
  Widget _searchField(BuildContext context) {
    return TextField(
      controller: textEditingController,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
          labelText: "Search",
          prefixIcon: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          suffixIcon: IconButton(
            icon: const Icon(Icons.clear),
            onPressed: () {
              setState(() {
                searchData.clear();
                textEditingController.text = '';
              });
            },
          )),
      onChanged: onSearchTextChanged,
    );
  }

  // List build
  _resultSearch(BuildContext context) {
    return Expanded(
      child: searchData.isEmpty
          ? ListView.builder(
              itemBuilder: (context, int index) {
                return _listEmpty(context);
              },
            )
          : ListView.builder(
              itemCount: searchData.length,
              itemBuilder: (context, int index) {
                Item item = fullData[index];
                return _resultList(context, index, item);
              },
            ),
    );
  }

  // Empty List
  Widget _listEmpty(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(),
        ],
      ),
    );
  }

  // List Result
  Widget _resultList(BuildContext context, int index, Item item) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => DetailItemFood(item, fullData)));
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        margin: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 2,
            ),
            Text(searchData[index].nameFood.toString())
          ],
        ),
      ),
    );
  }
}
