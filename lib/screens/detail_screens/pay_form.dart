import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import '../../common/common_styles.dart';
import '../../common/space_box.dart';

class PayForm extends StatefulWidget {
  final String tableCode;

  const PayForm(this.tableCode, {Key? key}) : super(key: key);

  @override
  State<PayForm> createState() => _PayFormState();
}

class _PayFormState extends State<PayForm> {
  DateTime _currentDate2 = DateTime(2019, 2, 3);
  String _currentMonth = DateFormat.yMMM().format(DateTime(2019, 2, 3));
  DateTime _targetDateTime = DateTime(2019, 2, 3);

  bool _isShow = false;
  bool _isShowCalendarCarousel = false;
  bool _button = false;
  int numberOfPeople = 1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: const Color(0xff111111),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _backgroundTitle(context),
                HeightSpaceBox.height30(),
                _name(context),
                HeightSpaceBox.height10(),
                _datePicker(context),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Divider(
                    color: Colors.white,
                    height: 40,
                    thickness: 1,
                  ),
                ),
                _guest(context),
                _buildTextFieldContactnumber(context),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Divider(
                    color: Colors.white,
                    height: 40,
                    thickness: 1,
                  ),
                ),
                _request(context),
                HeightSpaceBox.height40(),
                _reservation(context),
                HeightSpaceBox.height60(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _backgroundTitle(BuildContext context) {
    return Image.asset('assets/images/img_pay_bg.png');
  }

  Widget _name(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: TextField(
        style: CommonStyles.w400Size18Black(context),
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            hintText: 'Name',
            hintStyle: CommonStyles.w400Size18Black00(context),
            border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white))),
      ),
    );
  }

  Widget _datePicker(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            height: 60,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Date',
                  style: CommonStyles.w400Size18Black00(context),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _isShowCalendarCarousel = !_isShowCalendarCarousel;
                    });
                  },
                  child: const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ],
            ),
          ),
          HeightSpaceBox.height10(),
          _isShowCalendarCarousel
              ? Card(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 40, vertical: 20),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text(_currentMonth,
                                    style: CommonStyles.w600Size18Black00(
                                        context))),
                            GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _targetDateTime = DateTime(
                                        _targetDateTime.year,
                                        _targetDateTime.month - 1);
                                    _currentMonth = DateFormat.yMMM()
                                        .format(_targetDateTime);
                                  });
                                },
                                child: Container(
                                  height: 40,
                                  width: 40,
                                  alignment: Alignment.center,
                                  child: SvgPicture.asset(Resources.icPrevious,
                                      color: CommonColors.black00),
                                )),
                            const SizedBox(width: 20),
                            GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _targetDateTime = DateTime(
                                        _targetDateTime.year,
                                        _targetDateTime.month + 1);
                                    _currentMonth = DateFormat.yMMM()
                                        .format(_targetDateTime);
                                  });
                                },
                                child: Container(
                                  height: 40,
                                  width: 40,
                                  alignment: Alignment.center,
                                  child: SvgPicture.asset(Resources.icNextBold,
                                      color: CommonColors.black00),
                                )),
                          ],
                        ),
                      ),
                      CalendarCarousel<Event>(
                        onDayPressed: (date, events) {
                          setState(() => _currentDate2 = date);
                        },
                        showIconBehindDayText: true,
                        iconColor: CommonColors.black00,
                        headerTextStyle:
                            CommonStyles.w400Size16Black00(context),
                        selectedDayBorderColor: Colors.blue,
                        selectedDayButtonColor: Colors.blue,
                        selectedDayTextStyle:
                            CommonStyles.w400Size16Black00(context),
                        todayBorderColor: Colors.transparent,
                        weekdayTextStyle:
                            CommonStyles.w400Size16Black00(context),
                        height: 400,
                        daysHaveCircularBorder: true,
                        weekendTextStyle:
                            CommonStyles.w400Size16Black00(context),
                        showOnlyCurrentMonthDate: false,
                        selectedDateTime: _currentDate2,
                        targetDateTime: _targetDateTime,
                        customGridViewPhysics:
                            const NeverScrollableScrollPhysics(),
                        showHeader: false,
                        onCalendarChanged: (DateTime date) {
                          setState(() {
                            _targetDateTime = date;
                            _currentMonth =
                                DateFormat.yMMM().format(_targetDateTime);
                          });
                        },
                      ),
                    ],
                  ),
                )
              : const SizedBox(
                  height: 5,
                ),
        ],
      ),
    );
  }

  Widget _guest(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            height: 60,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Number of guests',
                  style: CommonStyles.w400Size18Black00(context),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _isShow = !_isShow;
                    });
                  },
                  child: const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ],
            ),
          ),
          HeightSpaceBox.height10(),
          _isShow
              ? Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  height: 160,
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 170,
                        height: 160,
                        child: Stack(
                          children: [
                            Container(
                              height: 160,
                              width: 170,
                              color: const Color(0xffe6e6e6),
                            ),
                            Center(
                                child:
                                    Image.asset('assets/icons/ic_ellipse.png')),
                            Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(bottom: 60),
                                child: Image.asset(
                                    'assets/icons/ic_person_down.png')),
                            Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(top: 60),
                                child: Image.asset(
                                    'assets/icons/ic_person_up.png')),
                          ],
                        ),
                      ),
                      WidthSpaceBox.width20(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          HeightSpaceBox.height20(),
                          Text(
                            'Table Reservation',
                            style: CommonStyles.w400Size18Black(context),
                          ),
                          HeightSpaceBox.height10(),
                          Text(
                            'Party of',
                            style: CommonStyles.w700Size22Black00(context),
                          ),
                          HeightSpaceBox.height20(),
                          Row(
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (numberOfPeople > 1) {
                                      --numberOfPeople;
                                    }
                                  });
                                },
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color(0xffebebeb)),
                                  child: Icon(
                                    Icons.remove,
                                    color: CommonColors.black00,
                                  ),
                                ),
                              ),
                              WidthSpaceBox.width20(),
                              Text(
                                numberOfPeople.toString(),
                                style: CommonStyles.w400Size26Black00(context),
                              ),
                              WidthSpaceBox.width20(),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    ++numberOfPeople;
                                  });
                                },
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color(0xffebebeb)),
                                  child: Icon(
                                    Icons.add,
                                    color: CommonColors.black00,
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                )
              : const SizedBox(
                  height: 10,
                ),
        ],
      ),
    );
  }

  Widget _request(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: TextField(
        maxLines: 5,
        style: CommonStyles.w400Size18Black(context),
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            hintText: 'Special requests',
            hintStyle: CommonStyles.w400Size18Black00(context),
            isDense: true,
            border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white))),
      ),
    );
  }

  Widget _buildTextFieldContactnumber(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: TextField(
        style: CommonStyles.w400Size18Black(context),
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            hintText: 'Contact number',
            hintStyle: CommonStyles.w400Size18Black00(context),
            border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white))),
      ),
    );
  }

  Widget _reservation(BuildContext context) {
    return _button
        ? Center(
            child: Container(
              child: Material(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      _button = false;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    alignment: Alignment.center,
                    child: const Text(
                      "Continue",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                color: Colors.transparent,
              ),
              decoration: BoxDecoration(
                  gradient: const LinearGradient(colors: [
                    Color(0xffffa013),
                    Color(0xfff4340a),
                  ]),
                  borderRadius: BorderRadius.circular(5)),
            ),
          )
        : Center(
            child: Container(
              child: Material(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      _button = true;
                    });
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            _reservationSuccess(context));
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    alignment: Alignment.center,
                    child: const Text(
                      "Make a reservation",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                color: Colors.transparent,
              ),
              decoration: BoxDecoration(
                  gradient: const LinearGradient(colors: [
                    Color(0xffffa013),
                    Color(0xfff4340a),
                  ]),
                  borderRadius: BorderRadius.circular(5)),
            ),
          );
  }

  Widget _reservationSuccess(BuildContext context) {
    return Dialog(
      backgroundColor: const Color(0xff5f5f5f),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: SizedBox(
        height: 500,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Text(
              'RESERVATION BOOKED!!',
              textAlign: TextAlign.center,
              style: CommonStyles.w700Size18Green54(context),
            ),
            Container(
                width: 60,
                height: 60,
                margin: const EdgeInsets.only(top: 40),
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: CommonColors.green54, width: 2)),
                child: SvgPicture.asset(
                  'assets/icons/ic_check_green.svg',
                )),
            const SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Name',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      ),
                      Text(
                        'Duc Thien Vu',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      )
                    ],
                  ),
                  HeightSpaceBox.height20(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Table',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      ),
                      Text(
                        '7',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      )
                    ],
                  ),
                  HeightSpaceBox.height20(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Time Reserve',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      ),
                      Text(
                        '17:30',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      )
                    ],
                  ),
                  HeightSpaceBox.height20(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Mobile',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      ),
                      Text(
                        '5145832803',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      )
                    ],
                  ),
                  HeightSpaceBox.height20(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Reservation ID',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      ),
                      Text(
                        '12344SD1531nf',
                        style: CommonStyles.w400Size18WhiteFF(context),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _reservationFail(BuildContext context) {
  //   return Dialog(
  //     backgroundColor: CommonColors.pinkE9,
  //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
  //     child: SizedBox(
  //       height: 240,
  //       width: 400,
  //       child: Column(
  //         children: [
  //           const SizedBox(
  //             height: 40,
  //           ),
  //           Text(
  //             'We are sorry!!',
  //             textAlign: TextAlign.center,
  //             style: CommonStyles.w700Size18RedFF(context),
  //           ),
  //           Text(
  //             'Restaurant is busy at the moment',
  //             textAlign: TextAlign.center,
  //             style: CommonStyles.w400Size14RedFF(context),
  //           ),
  //           Container(
  //               width: 60,
  //               height: 60,
  //               margin: const EdgeInsets.only(top: 40),
  //               padding: const EdgeInsets.all(15),
  //               decoration: BoxDecoration(
  //                   shape: BoxShape.circle,
  //                   border: Border.all(color: CommonColors.redFF, width: 2)),
  //               child: SvgPicture.asset(
  //                 'assets/icons/ic_close_red.svg',
  //               )),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
