import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_event.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/button.dart';
import 'package:flutter_base/common/ui/load_more_detail.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/item_bill.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class DetailItemDrink extends StatefulWidget {
  final Item item;
  final List<Item> listItem;

  const DetailItemDrink(this.item, this.listItem, {Key? key}) : super(key: key);

  @override
  _DetailItemDrinkState createState() => _DetailItemDrinkState();
}

class _DetailItemDrinkState extends State<DetailItemDrink> {

  final formatCurrency = NumberFormat.decimalPattern('vi');

  _requestAddItemBill(BuildContext context, ItemBill itemBill) {
    BlocProvider.of<BillBloc>(context).add(RequestAddItemBill(itemBill));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CommonColors.black11,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _buildBackGround(context, widget.item),
                _buildListLoadMore(context),
                _buildButton(context)
              ],
            ),
          ),
        ));
  }

  Widget _buildBackGround(BuildContext context, Item item) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.55,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.loose,
        children: [
          SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                Resources.imgBgrDetailItemFood,
                fit: BoxFit.fitWidth,
              )),
          Positioned(
              top: 40,
              left: 20,
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SvgPicture.asset(Resources.icBack))),
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(item.type!,
                  style: CommonStyles.w500Size24WhiteFF(context)),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: 250,
                width: 300,
                child: Stack(
                  children: [
                    Image.asset(item.pathImage!),
                    // _buildContent(context),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        color: CommonColors.black00.withOpacity(0.2),
                        width: MediaQuery.of(context).size.width * 0.5,
                        height: MediaQuery.of(context).size.height * 0.2,
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Text(item.nameFood!,
                                    style: CommonStyles.w700Size18WhiteFF(
                                        context)),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(item.describe!,
                                    maxLines: 3,
                                    overflow: TextOverflow.clip,
                                    style: CommonStyles.w400Size12WhiteFF(
                                        context)),
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: Text(
                                    '${formatCurrency.format(item.price)} VND',
                                    textAlign: TextAlign.end,
                                    style: CommonStyles.w700Size18OrangeF2(
                                        context)),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget _buildListLoadMore(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.25,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.listItem.length,
          itemBuilder: (context, index) {
            Item _item = widget.listItem[index];
            return LoadMoreDetail(context, _item, widget.listItem);
          },
        ));
  }

  Widget _buildButton(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 40,
          horizontal: 20,
        ),
        child: GestureDetector(
            onTap: () {
              _requestAddItemBill(context, addItemBill(widget.item));
              Navigator.pop(context);
            },
            child: Buttons.displayButton(context, 'Order', 10)));
  }

  ItemBill addItemBill(Item itemFoodDrink) {
    ItemBill itemBill = ItemBill(
        itemFoodDrink.nameFood,
        itemFoodDrink.pathImage,
        false,
        1,
        itemFoodDrink.price,
        itemFoodDrink.price,
        itemFoodDrink.discount);
    return itemBill;
  }
}
