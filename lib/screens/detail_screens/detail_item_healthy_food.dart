import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/load_more_food.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';

class DetailItemHealthyFood extends StatefulWidget {
  final List<Item> listItem;
  final  List<MoreItemModel> listMoreFoodDrink;

  const DetailItemHealthyFood(this.listItem, this.listMoreFoodDrink, {Key? key})
      : super(key: key);

  @override
  _DetailItemHealthyFoodState createState() => _DetailItemHealthyFoodState();
}

class _DetailItemHealthyFoodState extends State<DetailItemHealthyFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CommonColors.black11,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 50),
                  child: Column(
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.6,
                          child: Stack(
                            fit: StackFit.passthrough,
                            children: [
                              Image.asset(Resources.imgBgrHealthyList),
                              Padding(
                                padding: const EdgeInsets.only(top: 50),
                                child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Text(widget.listItem[0].type!,
                                        style: CommonStyles.w500Size24OrangeF2(
                                            context))),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 50, left: 20),
                                child: ListView.builder(
                                    itemCount: widget.listItem.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {
                                      Item _item = widget.listItem[index];
                                      return _buildItem(context, _item);
                                    }),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                LoadMoreFood(context, widget.listMoreFoodDrink)
              ],
            ),
          ),
        ));
  }

  Widget _buildItem(BuildContext context, Item item) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => DetailItemFood(item, widget.listItem)));
        },
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.6,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(child: Image.asset(item.pathImage!)),
              Text(item.nameFood!,
                  style: CommonStyles.w700Size18OrangeF2(context)),
              const SizedBox(height: 5),
              Text(
                item.describe!,
                maxLines: 3,
                overflow: TextOverflow.clip,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: CommonColors.whiteFF,
                    height: 2),
              )
            ],
          ),
        ),
      ),
    );
  }
}
