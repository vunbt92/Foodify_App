import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_event.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/button.dart';
import 'package:flutter_base/common/ui/load_more_detail.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/item_bill.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class DetailItemFood extends StatefulWidget {
  final Item item;
  final List<Item> listItem;

  const DetailItemFood(this.item, this.listItem, {Key? key}) : super(key: key);

  @override
  _DetailItemFoodState createState() => _DetailItemFoodState();
}

class _DetailItemFoodState extends State<DetailItemFood> {

  final formatCurrency = NumberFormat.decimalPattern('vi');

  _requestAddItemBill(BuildContext context, ItemBill itemBill) {
    BlocProvider.of<BillBloc>(context).add(RequestAddItemBill(itemBill));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CommonColors.black11,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _builBackGround(context),
                _buildContent(context, widget.item),
                _buildListLoadMore(context),
                _buildButton(context)
              ],
            ),
          ),
        ));
  }

  Widget _builBackGround(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.55,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.loose,
        children: [
          SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                Resources.imgBgrDetailItemFood,
                fit: BoxFit.fitWidth,
              )),
          Positioned(
              top: 40,
              left: 20,
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SvgPicture.asset(Resources.icBack))),
          Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  height: 250,
                  width: 300,
                  child: Image.asset(widget.item.pathImage!)))
        ],
      ),
    );
  }

  Widget _buildContent(BuildContext context, Item item) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
                alignment: Alignment.centerRight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(formatCurrency.format(item.price),
                        textAlign: TextAlign.end,
                        style: CommonStyles.w700Size22OrangeF2(context)),
                    Text('VND',
                        textAlign: TextAlign.end,
                        style: CommonStyles.w700Size22OrangeF2(context)),
                  ],
                )),
            Text(item.nameFood!,
                style: CommonStyles.w700Size24OrangeF3(context)),
            Text(item.tag!, style: CommonStyles.w400Size14WhiteFF(context)),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30),
              child: Divider(color: CommonColors.whiteFF, height: 0.5),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListLoadMore(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.25,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.listItem.length,
          itemBuilder: (context, index) {
            Item _item = widget.listItem[index];
            return LoadMoreDetail(context, _item, widget.listItem);
          },
        ));
  }

  Widget _buildButton(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 40,
          horizontal: 20,
        ),
        child: GestureDetector(
            onTap: () {
              _requestAddItemBill(context, addItemBill(widget.item));
              Navigator.pop(context);
            },
            child: Buttons.displayButton(context, 'Order', 10)));
  }

  ItemBill addItemBill(Item itemFoodDrink) {
    ItemBill itemBill = ItemBill(
        itemFoodDrink.nameFood,
        itemFoodDrink.pathImage,
        false,
        1,
        itemFoodDrink.price!,
        itemFoodDrink.price!,
        itemFoodDrink.discount);
    return itemBill;
  }
}
