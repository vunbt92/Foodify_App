import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScannerScreen extends StatefulWidget {
  const QRScannerScreen({Key? key}) : super(key: key);

  @override
  _QRScannerScreenState createState() => _QRScannerScreenState();
}

class _QRScannerScreenState extends State<QRScannerScreen> {
  final GlobalKey _qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? _result;
  QRViewController? _controller;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _controller!.pauseCamera();
    } else if (Platform.isIOS) {
      _controller!.resumeCamera();
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            QRView(
              key: _qrKey,
              overlay: QrScannerOverlayShape(
                borderColor: CommonColors.orangeFD,
                borderLength: 50,
                borderWidth: 15,
                cutOutSize: 300,
              ),
              onQRViewCreated: _onQRViewCreated,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () async {
                        await _controller?.toggleFlash();
                        setState(() {});
                      },
                      child: SvgPicture.asset(Resources.icFlash)),
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: SvgPicture.asset(Resources.icClose))
                ],
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.1,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Scan the code',
                        textAlign: TextAlign.center,
                        style: CommonStyles.w700Size20WhiteFF(context),
                      ),
                      const SizedBox(height: 10),
                      Text(
                          'Place the code close to scan your code for getting more info about our restaurant',
                          textAlign: TextAlign.center,
                          style: CommonStyles.w400Size16WhiteFF(context)),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  //
  // _handleMoveToHomeScreen(Barcode result) {
  //   Navigator.pushAndRemoveUntil(
  //       context,
  //       MaterialPageRoute(builder: (_) => PagesScreen(_result!)),
  //       (route) => false);
  // }

  void _onQRViewCreated(QRViewController controller) {
    _controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        _result = scanData;
      });
      if (_result!.code!.isNotEmpty) {
        controller.stopCamera();
        // _handleMoveToHomeScreen(_result!);
      }
    });
  }
}
