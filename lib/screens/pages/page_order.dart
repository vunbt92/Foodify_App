import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/button.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/bill_model.dart';
import 'package:flutter_base/models/item_bill.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class PageOrder extends StatefulWidget {
  final BillModel listItemBill;

  const PageOrder(this.listItemBill, {Key? key}) : super(key: key);

  @override
  _PageOrderState createState() => _PageOrderState();
}

class _PageOrderState extends State<PageOrder> {
  final formatCurrency = NumberFormat.decimalPattern('vi');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildListOrder(context),
            _buildTotalMoney(context),
            _buildButton(context)
          ],
        ),
      ),
    );
  }

  Widget _buildListOrder(BuildContext context) {
    widget.listItemBill.totalBill = 0;

    return Visibility(
      visible: widget.listItemBill.listBill.isNotEmpty,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: widget.listItemBill.listBill.map((item) {
            var priceDiscount = (item.price! * item.discount!.toInt()) / 100;
            var finalPrice = item.price! - priceDiscount;
            var currentItem = item;
            var indexItem = widget.listItemBill.listBill.indexOf(item);
            widget.listItemBill.totalBill =
                (widget.listItemBill.totalBill + finalPrice);
            return _buiItemOrder(context, currentItem, indexItem, finalPrice);
          }).toList(),
        ),
      ),
    );
  }

  Widget _buildTotalMoney(BuildContext context) {
    var _totalBill = (widget.listItemBill.totalBill);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Divider(
            height: 2,
            color: CommonColors.whiteFF,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total bill',
                  style: CommonStyles.w400Size24WhiteFF(context),
                ),
                Text(
                  '${formatCurrency.format(_totalBill)} VND',
                  style: CommonStyles.w700Size24WhiteFF(context),
                ),
              ],
            ),
          ),
          Divider(
            height: 2,
            color: CommonColors.whiteFF,
          ),
        ],
      ),
    );
  }

  Widget _buildButton(BuildContext context) {
    return Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 100, bottom: 50),
        child: GestureDetector(
          onTap: () {
            setState(() {
              widget.listItemBill.listBill.clear();
            });
            showDialog(
                context: context,
                builder: (BuildContext context) =>
                    widget.listItemBill.totalBill == 0
                        ? _orderFail(context)
                        : _orderDone(context));
          },
          child: Buttons.displayButton(context, 'Confirm order', 10),
        ));
  }

  Widget _buiItemOrder(
      BuildContext context, ItemBill item, int indexItem, double finalPrice) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(bottom: 20),
        child: Stack(
          children: [
            Positioned(
                right: 0,
                top: 5,
                child: GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.listItemBill.listBill.remove(item);
                      });
                    },
                    child: SvgPicture.asset(Resources.icBin))),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                width: MediaQuery.of(context).size.width,
                color: CommonColors.greyC4.withOpacity(0.7),
              ),
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  item.pathImage!,
                  height: 162,
                  width: 150,
                )),
            Align(
              alignment: Alignment.bottomRight,
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.15,
                width: MediaQuery.of(context).size.width * 0.55,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          item.nameFood!,
                          style: CommonStyles.w700Size14Black00(context),
                        ),
                        Text(
                          'Quantity',
                          style: CommonStyles.w400Size12Black00(context),
                        ),
                        Text('Price',
                            style: CommonStyles.w400Size12Black00(context)),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const Text(''),
                        Row(children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                if (item.quantity! > 1) {
                                  widget.listItemBill.listBill[indexItem]
                                      .quantity = (item.quantity! - 1);
                                  widget.listItemBill.listBill[indexItem]
                                      .price = (item.price! - item.firstPrice!);
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (context) =>
                                          _deleteProduct(context, item));
                                  // widget.listItemBill.listBill.remove(item);
                                }
                              });
                            },
                            child: Container(
                                height: 30,
                                width: 30,
                                alignment: Alignment.center,
                                decoration:
                                    BoxDecoration(color: CommonColors.whiteFF),
                                child: Text('-',
                                    style: CommonStyles.w700Size14Black00(
                                        context))),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: Text(item.quantity.toString(),
                                style: CommonStyles.w700Size14Black00(context)),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                widget.listItemBill.listBill[indexItem]
                                    .quantity = (item.quantity! + 1);
                                widget.listItemBill.listBill[indexItem].price =
                                    (item.price! + item.firstPrice!);
                              });
                            },
                            child: Container(
                                height: 30,
                                width: 30,
                                alignment: Alignment.center,
                                decoration:
                                    BoxDecoration(color: CommonColors.whiteFF),
                                child: Text('+',
                                    style: CommonStyles.w700Size14Black00(
                                        context))),
                          ),
                        ]),
                        Text('${formatCurrency.format(finalPrice)} VND',
                            style: CommonStyles.w700Size14Black00(context)),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  //Order done
  Widget _orderDone(BuildContext context) {
    return Dialog(
      backgroundColor: CommonColors.grey5F,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: SizedBox(
        height: 400,
        child: Column(
          children: [
            Container(
                padding: const EdgeInsets.only(top: 30),
                alignment: Alignment.topCenter,
                child: SvgPicture.asset('assets/images/img_order_success.svg')),
            const SizedBox(
              height: 60,
            ),
            Text(
              'Order was placed\nSuccessfully!',
              textAlign: TextAlign.center,
              style: CommonStyles.w700Size24OrangeFC(context),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'We’ve received your order and our team is\nworking to get it to you as quick as possible.',
              textAlign: TextAlign.center,
              style: CommonStyles.w500Size14Black29(context),
            ),
          ],
        ),
      ),
    );
  }

  //Order fail
  Widget _orderFail(BuildContext context) {
    return Dialog(
      backgroundColor: CommonColors.pinkFF,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: SizedBox(
        height: 180,
        width: 400,
        child: Column(
          children: [
            const SizedBox(
              height: 40,
            ),
            Text(
              'We are sorry!!',
              textAlign: TextAlign.center,
              style: CommonStyles.w700Size18OrangeFC(context),
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              'Due to the current high volume\nof orders ',
              textAlign: TextAlign.center,
              style: CommonStyles.w400Size14OrangeFC(context),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'your oder will be served soon!',
              textAlign: TextAlign.center,
              style: CommonStyles.w400Size14OrangeFC(context),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget _deleteProduct(BuildContext context, ItemBill item) {
    return AlertDialog(
      title: const Text('Do you want to delete?'),
      content: const Text('Are you sure?'),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context, 'Cancel'),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
              widget.listItemBill.listBill.remove(item);
            });
          },
          child: const Text('OK'),
        ),
      ],
    );
  }
}
