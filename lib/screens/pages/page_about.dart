import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/screens/feedback/feedback_screen.dart';

class PageAbout extends StatefulWidget {
  final String tableCode;

  const PageAbout(this.tableCode, {Key? key}) : super(key: key);

  @override
  _PageAboutState createState() => _PageAboutState();
}

class _PageAboutState extends State<PageAbout> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: Stack(
      fit: StackFit.passthrough,
      children: [
        Image.asset(
          Resources.imgBgrAboutPage,
          fit: BoxFit.cover,
        ),
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Foodify Version 1.0',
                  style: CommonStyles.w400Size16WhiteFF(context),
                ),
                const SizedBox(height: 10),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const FeedbackScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'Feedback',
                    style: CommonStyles.w400Size16WhiteFF(context),
                  ),
                ),
                // GestureDetector(
                //   onTap: () {
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //         builder: (BuildContext context) => PayForm(widget.tableCode),
                //       ),
                //     );
                //   },
                //   child: Text(
                //     'Payment',
                //     style: CommonStyles.w400Size16WhiteFF(context),
                //   ),
                // )
              ],
            )),
      ],
    ));
  }
}
