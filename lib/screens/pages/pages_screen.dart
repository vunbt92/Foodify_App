import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/bill_model.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/screens/pages/page_about.dart';
import 'package:flutter_base/screens/pages/page_menu.dart';
import 'package:flutter_base/screens/pages/page_order.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PagesScreen extends StatefulWidget {
  // final Barcode result;
  final String idTable;

  const PagesScreen(this.idTable, {Key? key}) : super(key: key);

  @override
  _PagesScreenState createState() => _PagesScreenState();
}

class _PagesScreenState extends State<PagesScreen>
    with SingleTickerProviderStateMixin {
  late final BillModel _listItemBill;
  final List<Data> _listDataItemFoodDrink = [];

  // bool _isSearching = false;
  String? _tableCode;
  bool _isSame = false;

  FoodAndDrink _foodAndDrink = FoodAndDrink();

  // Tab controller
  late final TabController _tabController;

  // Index selected menu screen
  int _indexSelected = 0;

  // Child page controller
  PageController? _pageController;

  @override
  void initState() {
    // _tableCode = widget.result.code!;
    _tableCode = widget.idTable;
    _listItemBill = BillModel(widget.idTable, 0, 30, []);
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // Display popup when backPressed
  Future<bool> _onWillPop() async {
    if (_indexSelected != 0 &&
        _tabController.index == 0 &&
        _pageController != null) {
      _pageController?.animateToPage(0,
          duration: const Duration(milliseconds: 200), curve: Curves.easeIn);
      return false;
    }
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sure?'),
            content: const Text('Do you want to exit an App.'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BillBloc, BillState>(
        builder: (context, state) => _buildUI(context),
        listener: (context, state) {
          if (state is BillLoading) {
            log('Loading...');
          } else if (state is ItemBillLoaded) {
            if (_listItemBill.listBill.isNotEmpty) {
              for (var element in _listItemBill.listBill) {
                if (element.nameFood!
                    .contains(state.itemBill.nameFood.toString())) {
                  _isSame = true;
                  element.quantity = element.quantity! + 1;
                  element.price = element.price! + element.firstPrice!;
                  break;
                } else {
                  _isSame = false;
                }
              }
            } else {
              _isSame = false;
            }
            if (_isSame == false) {
              _listItemBill.listBill.add(state.itemBill);
            }
          } else if (state is BillLoadError) {
            log('AAA:${state.message}');
          } else if (state is ItemFoodDrinkLoading) {
            log('Loading...');
          } else if (state is ItemFoodDrinkLoaded) {
            _foodAndDrink = state.foodAndDrink;
            _foodAndDrink.data?.forEach((element) {
              _listDataItemFoodDrink.add(element);
            });
          } else if (state is ItemFoodDrinkError) {
            log('AAA:${state.message}');
          }
        });
  }

  Widget _buildUI(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          backgroundColor: CommonColors.black11,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text('Table: #${widget.idTable.split('=')[1]}',
                style: CommonStyles.w700Size24OrangeF3(context)),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    Resources.imgBgrPages,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            backgroundColor: CommonColors.black11,
            bottom: TabBar(
                controller: _tabController,
                unselectedLabelStyle: CommonStyles.w400Size14WhiteEC(context),
                labelStyle: CommonStyles.w400Size14Grey(context),
                indicator: UnderlineTabIndicator(
                  insets: const EdgeInsets.only(bottom: 10),
                  borderSide: BorderSide(
                    color: CommonColors.redFF,
                    width: 4.0,
                  ),
                ),
                tabs: const [
                  Tab(child: Text('Menu')),
                  Tab(child: Text('Order')),
                  Tab(child: Text('About'))
                ]),
          ),
          body: SafeArea(child: _buildTabBarView(context))),
    );
  }

  Widget _buildTabBarView(BuildContext context) {
    return TabBarView(controller: _tabController, children: [
      PageMenu(
        _listDataItemFoodDrink,
        onPageSelected: (index, PageController controller) {
          setState(() {
            _indexSelected = index;
            _pageController = controller;
          });
        },
      ),
      PageOrder(_listItemBill),
      PageAbout(_tableCode!),
    ]);
  }

// Widget _buildSearchBar(BuildContext context) {
//   return _isSearching == false
//       ? Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 30),
//           child: GestureDetector(
//               onTap: () {
//                 setState(() {
//                   _isSearching = !_isSearching;
//                 });
//               },
//               child: SvgPicture.asset(Resources.icSearch)),
//         )
//       : SizedBox(
//           width: MediaQuery.of(context).size.width,
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 20),
//             child: TextField(
//                 onChanged: (value) => _handleRequestSearch(value),
//                 style: CommonStyles.w400Size18WhiteFF(context),
//                 autofocus: false,
//                 decoration: InputDecoration(
//                   suffixIconConstraints:
//                       const BoxConstraints(minHeight: 24, minWidth: 24),
//                   hintText: 'Search...',
//                   hintStyle: CommonStyles.w400Size18Grey(context),
//                   suffixIcon: GestureDetector(
//                     onTap: () {
//                       setState(() {
//                         _isSearching = !_isSearching;
//                         if (_isSearching == false) {
//                           _handleRequestSearch("");
//                         }
//                       });
//                     },
//                     child: Container(
//                       height: 40,
//                       width: 40,
//                       alignment: Alignment.center,
//                       child: SvgPicture.asset(
//                         Resources.icClose,
//                         height: 15,
//                         width: 15,
//                       ),
//                     ),
//                   ),
//                   fillColor: CommonColors.grey5F.withOpacity(0.3),
//                   filled: true,
//                   border: InputBorder.none,
//                   focusedBorder: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(25.0),
//                       borderSide: BorderSide.none),
//                   enabledBorder: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(25.0),
//                       borderSide: BorderSide.none),
//                 )),
//           ),
//         );
// }
}
