import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_bloc.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_event.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/blocs/page/page_bloc.dart';
import 'package:flutter_base/blocs/page/page_state.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/page_menu_item.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';
import 'package:flutter_base/screens/menu/bar_screen.dart';
import 'package:flutter_base/screens/menu/desert_screen.dart';
import 'package:flutter_base/screens/menu/healthy_screen.dart';
import 'package:flutter_base/screens/menu/kitchen_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class PageMenu extends StatefulWidget {
  final Function onPageSelected;
  final List<Data> listDataItemFoodDrink;

  const PageMenu(this.listDataItemFoodDrink,
      {Key? key, required this.onPageSelected})
      : super(key: key);

  @override
  _PageMenuState createState() => _PageMenuState();
}

class _PageMenuState extends State<PageMenu>
    with AutomaticKeepAliveClientMixin {
  // Page controller
  final _discountPageController = PageController(initialPage: 0, keepPage: true);

  // Item selected
  Data? _itemSelected;

  // pageScreenController
  final _pageScreenController = PageController(initialPage: 0, keepPage: true);

  // Discounts
  final List<Item> _listItemDiscount = [];

  final List<MoreItemModel> _listMoreFoodDrink = [];

  _handleGetItemFoodDrink() {
    BlocProvider.of<BillBloc>(context).add(const RequestGetItemFoodDrink());
  }

  _handleGetItemDiscount() {
    BlocProvider.of<BillBloc>(context).add(const RequestGetItemDiscount());
  }

  @override
  void initState() {
    _handleGetItemDiscount();
    _handleGetItemFoodDrink();

    _pageScreenController.addListener(() {});
    super.initState();
  }

  @override
  void dispose() {
    // Release controllers
    _discountPageController.dispose();
    _pageScreenController.dispose();
    super.dispose();
  }

  // Handle when page changed
  _onPageChanged(int index) {
    widget.onPageSelected(index, _pageScreenController);
  }

  // Get type selected
  int _getTypeItemSelected() {
    switch (_itemSelected?.key) {
      case 'kitchen':
        return 1;
      case 'bar':
        return 2;
      case 'desert':
        return 3;
      case 'healthy':
        return 4;
    }
    return 0;
  }

  _handleGetItemSelected(String keyPage) {
    for (var element in widget.listDataItemFoodDrink) {
      if (element.key
          .toString()
          .toLowerCase()
          .contains(keyPage.toLowerCase().toString())) {
        _itemSelected = element;
        setState(() {});
        _getTypeItemSelected();
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocConsumer<BillBloc, BillState>(
        builder: (context, state) => _buildUI(context),
        listener: (context, state) {
          if (state is ItemDiscountLoading) {
            log('Loading...');
          } else if (state is ItemDiscountLoaded) {
            _listItemDiscount.addAll(state.itemDiscount);
          } else if (state is ItemDiscountError) {
            log('AAA:${state.message}');
          }
        });
  }

  Widget _buildUI(BuildContext context) {
    return BlocConsumer<PageBloc, PageState>(
        builder: (context, state) => _buildPageView(context),
        listener: (context, state) {
          if (state is PageLoading) {
            log('Loading...');
          } else if (state is PageLoaded) {
            _handleGetItemSelected(state.moreItemModel.key!);
          } else if (state is PageLoadError) {
            log('AAA:${state.message}');
          }
        });
  }

  // Menu main screen
  Widget _buildMenuScreen(BuildContext context) {
    return Container(
      color: CommonColors.black11,
      child: SingleChildScrollView(
        child: Column(
          children: [_buildListDiscount(context), _buildListMenu(context)],
        ),
      ),
    );
  }

  // Create pageView to handle UI switcher
  Widget _buildPageView(BuildContext context) {
    return PageView(
      controller: _pageScreenController,
      physics: const NeverScrollableScrollPhysics(),
      onPageChanged: _onPageChanged,
      children: [
        // Page 1
        _buildMenuScreen(context),
        // Page 2
        _displayScreenByCategory(context)
      ],
    );
  }

  // Display screen by category selected
  Widget _displayScreenByCategory(BuildContext context) {
    switch (_getTypeItemSelected()) {
      case 1:
        return KitchenScreen(_itemSelected, _listMoreFoodDrink);
      case 2:
        return BarScreen(_itemSelected, _listMoreFoodDrink);
      case 3:
        return DesertScreen(_itemSelected, _listMoreFoodDrink);
      case 4:
        return HealthyScreen(_itemSelected, _listMoreFoodDrink);
      default:
        return Container();
    }
  }

  // Build header discount
  Widget _buildListDiscount(BuildContext context) {
    return _listItemDiscount.isEmpty
        ? Container()
        : Column(children: <Widget>[
            SizedBox(
              height: 204,
              child: PageView.builder(
                itemCount: _listItemDiscount.length,
                controller: _discountPageController,
                itemBuilder: (context, index) {
                  var item = _listItemDiscount[index];
                  return _buildItemDiscount(context, item, index);
                },
              ),
            ),
            SmoothPageIndicator(
                controller: _discountPageController,
                // PageController
                count: _listItemDiscount.length,
                effect: ScrollingDotsEffect(
                    maxVisibleDots: 5,
                    dotHeight: 8,
                    dotWidth: 8,
                    activeDotColor: CommonColors.orangeF2,
                    dotColor: CommonColors.orangeF2.withOpacity(0.4)),
                onDotClicked: (index) {})
          ]);
  }

  // Build list menu
  Widget _buildListMenu(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        itemCount: widget.listDataItemFoodDrink.length,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          Data item = widget.listDataItemFoodDrink[index];
          if (_listMoreFoodDrink.isEmpty ||
              _listMoreFoodDrink.length < widget.listDataItemFoodDrink.length) {
            MoreItemModel moreItemModel =
                MoreItemModel(item.key, item.name, item.pathImage);
            _listMoreFoodDrink.add(moreItemModel);
          }
          return GestureDetector(
              onTap: () {
                setState(() {
                  _itemSelected = item;
                });
                _moveToPage(1);
              },
              child: PageMenuItem(item));
        });
  }

  // Build item discount
  Widget _buildItemDiscount(BuildContext context, Item item, int index) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                DetailItemFood(item, _listItemDiscount),
          ),
        );
      },
      child: Stack(
        children: [
          Image.asset(
            item.pathImage.toString(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 5),
            child: Container(
              alignment: Alignment.center,
              height: 35,
              width: MediaQuery.of(context).size.width * 0.65,
              color: CommonColors.orangeF2,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  '${item.discount}% discount on all dishes',
                  style: CommonStyles.w400Size18WhiteFF(context),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _moveToPage(int index) {
    _pageScreenController.animateToPage(index,
        duration: const Duration(milliseconds: 200), curve: Curves.easeIn);
  }

  @override
  bool get wantKeepAlive => true;
}
