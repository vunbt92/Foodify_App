import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/ui/button.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/screens/pages/pages_screen.dart';

class StartScreen extends StatefulWidget {
  // Parameter
  final String parameter;
  const StartScreen({Key? key, required this.parameter}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  late double height;
  late double width;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: CommonColors.black11,
      body: SafeArea(
        child: SizedBox(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildBody(context),
              _buildButton(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: height * 0.15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              Resources.imgBgrStart,
              height: height * 0.45,
              width: width * 0.8,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Enjoy your food ',
                style: CommonStyles.w700Size36WhiteFF(context),
              ),
            ),
            Text(
              'Good cuisine makes good people.',
              style: CommonStyles.w400Size16OrangeF2(context),
            )
          ],
        ));
  }

  Widget _buildButton(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => PagesScreen(widget.parameter),
                ),
              );
            },
            child: Buttons.displayButton(context, 'Continue', 10)));
  }
}
