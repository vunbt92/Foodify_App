import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/screens/start/start_screen.dart';

class SplashScreen extends StatefulWidget {
  // Parameter
  final String parameter;
  const SplashScreen({Key? key, required this.parameter}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    _checkData();
    super.initState();
  }

  _checkData() async {
    Future.delayed(const Duration(seconds: 2), _moveToNewScreen);
  }

  _moveToNewScreen() async {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (_) => StartScreen(parameter: widget.parameter,)),
        (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CommonColors.black11,
      body: SafeArea(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                Resources.imgBgrSplash1,
                height: 80,
                width: MediaQuery.of(context).size.width * 0.8,
              ),
              const SizedBox(height: 10),
              Image.asset(
                Resources.imgBgrSplash2,
                height: 40,
                width: MediaQuery.of(context).size.width * 0.8,
              )
            ],
          ),
        ),
      ),
    );
  }
}
