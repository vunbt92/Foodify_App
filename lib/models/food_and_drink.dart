class FoodAndDrink {
  List<Data>? data;

  FoodAndDrink({this.data});

  FoodAndDrink.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String? key;
  String? name;
  String? pathImage;
  String? title;
  String? content;
  double? totalVote;
  int? disCount;
  List<Items>? items;

  Data(
      {this.key,
      this.name,
      this.pathImage,
      this.title,
      this.content,
      this.totalVote,
      this.disCount,
      this.items});

  Data.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    name = json['name'];
    pathImage = json['pathImage'];
    title = json['title'];
    content = json['content'];
    totalVote = json['totalVote'];
    disCount = json['disCount'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['key'] = key;
    data['name'] = name;
    data['pathImage'] = pathImage;
    data['title'] = title;
    data['content'] = content;
    data['totalVote'] = totalVote;
    data['disCount'] = disCount;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? keyItem;
  List<Item>? item;

  Items({this.keyItem, this.item});

  Items.fromJson(Map<String, dynamic> json) {
    keyItem = json['keyItem'];
    if (json['item'] != null) {
      item = <Item>[];
      json['item'].forEach((v) {
        item!.add(Item.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['keyItem'] = keyItem;
    if (item != null) {
      data['item'] = item!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Item {
  String? nameFood;
  String? type;
  String? tag;
  String? pathImage;
  String? describe;
  int? rating;
  int? quantity;
  int? discount;
  int? price;

  Item(
      {this.nameFood,
      this.type,
      this.tag,
      this.pathImage,
      this.describe,
      this.rating,
      this.quantity,
      this.discount,
      this.price});

  Item.fromJson(Map<String, dynamic> json) {
    nameFood = json['nameFood'];
    type = json['type'];
    tag = json['tag'];
    pathImage = json['pathImage'];
    describe = json['describe'];
    rating = json['rating'];
    quantity = json['quantity'];
    discount = json['discount'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['nameFood'] = nameFood;
    data['type'] = type;
    data['tag'] = tag;
    data['pathImage'] = pathImage;
    data['describe'] = describe;
    data['rating'] = rating;
    data['quantity'] = quantity;
    data['discount'] = discount;
    data['price'] = price;
    return data;
  }
}
