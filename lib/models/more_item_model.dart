class MoreItemModel {
  final String? key;
  final String? name;
  final String? pathImage;
  MoreItemModel(this.key, this.name, this.pathImage);
}
