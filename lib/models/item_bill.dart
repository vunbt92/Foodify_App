class ItemBill {
  String? nameFood;
  String? pathImage;
  bool? status;
  int? quantity;
  int? firstPrice;
  int? price;
  int? discount;

  ItemBill(this.nameFood, this.pathImage, this.status, this.quantity,this.firstPrice, this.price, this.discount);
}
