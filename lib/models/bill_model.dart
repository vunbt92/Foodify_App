import 'package:flutter_base/models/item_bill.dart';

class BillModel {
  String id;
  double totalBill;
  int discount;
  List<ItemBill> listBill;

  BillModel(this.id, this.totalBill, this.discount, this.listBill);
}
