import 'package:flutter/material.dart';

class Routes {
  static final Routes singleton = Routes._internal();
  static const String pageMenuScreen = "page_menu";
  static const String kitchenScreen = "kitchen";
  static const String barScreen = "bar";
  static const String desertScreen = "desert";
  static const String healthyScreen = "healthy";

  factory Routes() {
    return singleton;
  }

  Routes._internal();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
}
