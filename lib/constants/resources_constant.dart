class Resources {
  static const String imgBgrSplash1 = 'assets/images/img_bgr_splash1.png';
  static const String imgBgrSplash2 = 'assets/images/img_bgr_splash2.png';
  static const String imgBgrStart = 'assets/images/img_bgr_start.png';
  static const String imgBgrPages = 'assets/images/img_bgr_pages.png';
  static const String imgItemMenu1 = 'assets/images/img_item_menu1.png';
  static const String imgBgrDiscount1 = 'assets/images/img_bgr_discount1.png';
  static const String imgBgrAboutPage = 'assets/images/img_bgr_about_page.png';
  static const String imgBgrFeedback = 'assets/images/img_bgr_feedback.png';
  static const String imgItemOrder = 'assets/images/img_item_order.png';
  static const String imgItemDesertFood =
      'assets/images/img_item_desert_food.png';
  static const String imgItemBarDrink = 'assets/images/img_item_bar_drink.png';
  static const String imgBgrDetailItemFood =
      'assets/images/img_bgr_detail_item_food.png';
  static const String imgBgrDetailItemDrink =
      'assets/images/img_bgr_detail_item_drink.png';
  static const String imgBgrHealthyList =
      'assets/images/img_bgr_healthy_list.png';
  static const String imgLoadMoreFood = 'assets/images/img_load_more_food.png';
  static const String imgItemHealthyFood =
      'assets/images/img_item_healthy_food.png';
  static const String imgItemKitchenFood =
      'assets/images/img_item_kitchen_food.png';
  static const String icSearch = 'assets/icons/ic_search.svg';
  static const String icNext = 'assets/icons/ic_next.svg';
  static const String icNextBold = 'assets/icons/ic_next_bold.svg';
  static const String icPrevious = 'assets/icons/ic_previous.svg';
  static const String icBack = 'assets/icons/ic_back.svg';
  static const String icBin = 'assets/icons/ic_bin.svg';
  static const String icClose = 'assets/icons/ic_close.svg';
  static const String icFlash = 'assets/icons/ic_flash.svg';
  static const String icRatingStart1 = 'assets/icons/ic_rating_start1.svg';
  static const String icRatingStart2 = 'assets/icons/ic_rating_start2.svg';
  static const String icRatingStart3 = 'assets/icons/ic_rating_start3.svg';
  static const String icStartVote = 'assets/icons/ic_start_vote.svg';
}
