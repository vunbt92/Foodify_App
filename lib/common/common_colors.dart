
import 'package:flutter/material.dart';

class CommonColors {
  static Color black00 = const Color(0xff000000);
  static Color black11 = const Color(0xff111111);
  static Color black29 = const Color(0xff292929);
  static Color black61 = const Color(0xff616060);
  static Color grey89 = const Color(0xff898989);
  static Color greyC4 = const Color(0xffC4C4C4);
  static Color whiteFF = const Color(0xffFFFFFF);
  static Color whiteEC = const Color(0xffECF3FE);
  static Color redFF = const Color(0xffFF2F01);
  static Color orangeFC = const Color(0xffFC8810);
  static Color orangeF2 = const Color(0xffF25C05);
  static Color orangeF3 = const Color(0xffF38100);
  static Color orangeFD = const Color(0xffFD8F11);
  static Color green54 = const Color(0xff54FF04);
  static Color pinkE9 = const Color(0xffE9CACA);
  static Color pinkFF = const Color(0xffFFECD7);
  static Color grey5F = const Color(0xff5F5F5F);
}