import 'package:flutter/material.dart';

class HeightSpaceBox {
  static SizedBox height60() {
    return const SizedBox(
      height: 60,
    );
  }

  static SizedBox height40() {
    return const SizedBox(
      height: 40,
    );
  }

  static SizedBox height30() {
    return const SizedBox(
      height: 30,
    );
  }

  static SizedBox height35() {
    return const SizedBox(
      height: 35,
    );
  }

  static SizedBox height20() {
    return const SizedBox(
      height: 20,
    );
  }

  static SizedBox height10() {
    return const SizedBox(
      height: 10,
    );
  }

  static SizedBox height100() {
    return const SizedBox(
      height: 100,
    );
  }
}

class WidthSpaceBox {
  static SizedBox width10() {
    return const SizedBox(
      width: 10,
    );
  }
  static SizedBox width20() {
    return const SizedBox(
      width: 20,
    );
  }
}
