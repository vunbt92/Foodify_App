import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/cards.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_healthy_food.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UiItemListHealthy extends StatefulWidget {
  final Items items;
  final String keyLastItem;
  final List<MoreItemModel> listMoreFoodDrink;

  const UiItemListHealthy(BuildContext context, this.items, this.keyLastItem,
      this.listMoreFoodDrink,
      {Key? key})
      : super(key: key);

  @override
  _UiItemListHealthyState createState() => _UiItemListHealthyState();
}

class _UiItemListHealthyState extends State<UiItemListHealthy> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildListFood(context);
  }

  Widget _buildListFood(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 50),
      child: Column(
        children: [
          SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              child: Stack(
                fit: StackFit.passthrough,
                children: [
                  Image.asset(Resources.imgBgrHealthyList),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(widget.items.item![0].type!,
                            style: CommonStyles.w500Size24OrangeF2(context))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50, left: 20),
                    child: ListView.builder(
                        itemCount: widget.items.item!.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          Item _item = widget.items.item![index];
                          return _buildItem(context, _item);
                        }),
                  )
                ],
              )),
          _buildMoreItem(
              context,
              widget.keyLastItem.compareTo(widget.items.keyItem!) == 0
                  ? false
                  : true)
        ],
      ),
    );
  }

  Widget _buildMoreItem(BuildContext context, bool visibility) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => DetailItemHealthyFood(
                          widget.items.item!, widget.listMoreFoodDrink),
                    ),
                  );
                },
                child: Text('See More',
                    style: CommonStyles.w500Size12OrangeF2(context))),
            const SizedBox(width: 5),
            SvgPicture.asset(Resources.icNext, color: CommonColors.orangeF2)
          ],
        ),
        const SizedBox(height: 40),
        Visibility(
          visible: visibility ? true : false,
          child: Divider(
            color: CommonColors.whiteFF,
            height: 0.5,
            indent: 20,
            endIndent: 20,
          ),
        ),
      ],
    );
  }

  Widget _buildItem(BuildContext context, Item item) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => DetailItemFood(item, widget.items.item!)));
        },
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.6,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Center(child: Image.asset(item.pathImage!)),
                  Positioned(
                      top: 50,
                      left: 10,
                      child: Cards.displayCardDiscount(context, item.discount!))
                ],
              ),
              Text(item.nameFood!,
                  style: CommonStyles.w700Size18OrangeF2(context)),
              const SizedBox(height: 5),
              Text(
                item.describe!,
                maxLines: 3,
                overflow: TextOverflow.clip,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: CommonColors.whiteFF,
                    height: 2),
              )
            ],
          ),
        ),
      ),
    );
  }
}
