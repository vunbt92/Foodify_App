import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/models/food_and_drink.dart';

class FoodItem extends StatelessWidget {
  final Item item;
  final Function onClickItem;

  const FoodItem({Key? key, required this.item, required this.onClickItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClickItem(),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.35,
        width: MediaQuery.of(context).size.width * 0.5,
        color: CommonColors.grey89.withOpacity(0.2),
        clipBehavior: Clip.none,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 7,
              child: Align(
                alignment: Alignment.center,
                child: Image.asset(
                  item.pathImage!,
                  height: 205,
                  width: 155,
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.nameFood!,
                          style: CommonStyles.w500Size18WhiteFF(context)),
                      const SizedBox(height: 5),
                      Text(
                        item.tag!,
                        maxLines: 2,
                        style: CommonStyles.w400Size12WhiteFF(context),
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
