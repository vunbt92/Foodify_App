import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';

class LoadMoreDetail extends StatefulWidget {
  final Item item;
  final List<Item> listItem;

  const LoadMoreDetail(BuildContext context, this.item, this.listItem,
      {Key? key})
      : super(key: key);

  @override
  _LoadMoreDetailState createState() => _LoadMoreDetailState();
}

class _LoadMoreDetailState extends State<LoadMoreDetail> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                DetailItemFood(widget.item, widget.listItem),
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        margin: const EdgeInsets.only(left: 10, right: 10, bottom: 50),
        decoration: BoxDecoration(
          color: CommonColors.black11,
          boxShadow: [
            BoxShadow(
              color: CommonColors.black00.withOpacity(0.4),
              spreadRadius: 4,
              blurRadius: 3,
              offset: const Offset(3, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Image.asset(
                  widget.item.pathImage!,
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Expanded(
              flex: 6,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.item.nameFood!,
                        textAlign: TextAlign.center,
                        style: CommonStyles.w500Size14WhiteFF(context)),
                    const SizedBox(height: 10),
                    Text(widget.item.price!.toInt().toString(),
                        textAlign: TextAlign.center,
                        style: CommonStyles.w700Size14WhiteFF(context)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
