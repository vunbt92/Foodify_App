import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/food_item.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_drink.dart';
import 'package:flutter_base/screens/detail_screens/detail_item_food.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UiItemList extends StatefulWidget {
  final Items items;
  final String typeList;

  const UiItemList(BuildContext context, this.items, this.typeList, {Key? key})
      : super(key: key);

  @override
  _UiItemListState createState() => _UiItemListState();
}

class _UiItemListState extends State<UiItemList> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(widget.items.item![0].type!,
                  style: CommonStyles.w500Size18WhiteFF(context)),
              SvgPicture.asset(Resources.icNext, color: CommonColors.whiteFF)
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.35,
            child: ListView.builder(
              itemCount: widget.items.item!.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                Item _item = widget.items.item![index];
                if (_item.discount != null && _item.discount != 0) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 10, left: 10, right: 5),
                    child: Banner(
                        location: BannerLocation.topStart,
                        message: '${_item.discount.toString()} %',
                        child: FoodItem(
                            item: _item,
                            onClickItem: () => _handleOnClickFoodItem(_item))),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 5),
                  child: FoodItem(
                      item: _item,
                      onClickItem: () => _handleOnClickFoodItem(_item)),
                );
                // return _buildItem(context, _item);
              },
              // separatorBuilder: (BuildContext context, int index) =>
              //     Container(
              //       width: 10,
              //     )
            ),
          )
        ],
      ),
    );
  }

  // Handle onClick food item
  _handleOnClickFoodItem(Item item) {
    Navigator.push(context, MaterialPageRoute(builder: (_) {
      if (widget.typeList.compareTo('drink') == 0) {
        return DetailItemDrink(item, widget.items.item!);
      }
      return DetailItemFood(item, widget.items.item!);
    }));
  }
}
