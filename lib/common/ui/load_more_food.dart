import 'package:flutter/material.dart';
import 'package:flutter_base/blocs/page/page_bloc.dart';
import 'package:flutter_base/blocs/page/page_even.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_base/models/more_item_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoadMoreFood extends StatefulWidget {
  final List<MoreItemModel> listMoreFoodDrink;

  const LoadMoreFood(BuildContext context, this.listMoreFoodDrink, {Key? key})
      : super(key: key);

  @override
  _LoadMoreFoodState createState() => _LoadMoreFoodState();
}

class _LoadMoreFoodState extends State<LoadMoreFood> {
  @override
  Widget build(BuildContext context) {
    return _buildAlsoView(context);
  }

  Widget _buildAlsoView(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              const EdgeInsets.only(bottom: 10, left: 30, right: 30, top: 50),
          child: Divider(color: CommonColors.whiteFF, thickness: 1),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: Text('', style: CommonStyles.w400Size12Black00(context)),
              ),
              Expanded(
                child: Text('Also view',
                    textAlign: TextAlign.center,
                    style: CommonStyles.w400Size18WhiteFF(context)),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text('See All',
                        textAlign: TextAlign.end,
                        style: CommonStyles.w500Size12OrangeF2(context)),
                    const SizedBox(
                      width: 5,
                    ),
                    SvgPicture.asset(Resources.icNext)
                  ],
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 190,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
              itemCount: widget.listMoreFoodDrink.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                var item = widget.listMoreFoodDrink[index];
                return _buildOtherFoods(context, item);
              }),
        )
      ],
    );
  }

  Widget _buildOtherFoods(BuildContext context, MoreItemModel item) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<PageBloc>(context).add(RequestChangePage(item));
      },
      child: Container(
        margin: const EdgeInsets.only(left: 6, right: 6, bottom: 50),
        decoration: BoxDecoration(
          color: CommonColors.black11,
          boxShadow: [
            BoxShadow(
              color: CommonColors.black00.withOpacity(0.4),
              spreadRadius: 4,
              blurRadius: 3,
              offset: const Offset(3, 3), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Image.asset(
                item.pathImage!,
                height: 105,
                width: 125,
                fit: BoxFit.contain,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Text(item.name!,
                  textAlign: TextAlign.center,
                  style: CommonStyles.w700Size14WhiteFF(context)),
            ),
          ],
        ),
      ),
    );
  }
}
