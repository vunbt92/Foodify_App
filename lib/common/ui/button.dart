import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_styles.dart';

class Buttons {
  static displayButton(BuildContext context, String text, double border) {
    return Container(
      clipBehavior: Clip.antiAlias,
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height * 0.07,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(border)),
          gradient: const LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromRGBO(255, 160, 19, 1),
              Color.fromRGBO(244, 52, 10, 1),
            ],
          )),
      child: Text(
        text,
        style: CommonStyles.w400Size24WhiteFF(context),
      ),
    );
  }
}
