import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/cards.dart';
import 'package:flutter_base/models/food_and_drink.dart';

class BannerScreens extends StatefulWidget {
  final Data itemMenu;

  const BannerScreens(BuildContext context, this.itemMenu, {Key? key})
      : super(key: key);

  @override
  _BannerScreensState createState() => _BannerScreensState();
}

class _BannerScreensState extends State<BannerScreens> {
  @override
  Widget build(BuildContext context) {
    return _buildBanner(context, widget.itemMenu);
  }

  Widget _buildBanner(BuildContext context, Data item) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
          height: 230,
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset(item.pathImage!),
                  Padding(
                      padding: const EdgeInsets.only(top: 10, left: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Cards.displayCardTitle(context, item.name!),
                          const SizedBox(height: 5),
                          Visibility(
                              visible: item.disCount == 0 ? false : true,
                              child: Cards.displayCardDiscount(
                                  context, item.disCount!))
                        ],
                      )),
                  Positioned(
                      top: 0,
                      right: 0,
                      child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.clear,
                            color: CommonColors.orangeFC,
                          ))),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(item.title!,
                      style: CommonStyles.w400Size18WhiteFF(context)),
                  Cards.displayCardVote(context, item.totalVote.toString())
                ],
              ),
              const SizedBox(height: 5),
              Text(
                item.content!,
                style: CommonStyles.w400Size12WhiteFF(context),
              )
            ],
          )),
    );
  }
}
