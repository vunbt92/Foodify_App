import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/constants/resources_constant.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Cards {
  static displayCardTitle(BuildContext context, String text) {
    return GestureDetector(
      onTap: () {},
      child: Point(
        triangleHeight: 10.0,
        edge: Edge.RIGHT,
        child: Container(
          alignment: Alignment.center,
          width: 135,
          height: 35,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              CommonColors.orangeF2,
              CommonColors.redFF,
            ],
          )),
          child: Text(
            text,
            style: CommonStyles.w500Size14WhiteFF(context),
          ),
        ),
      ),
    );
  }

  static displayCardDiscount(BuildContext context, int discount) {
    return Visibility(visible: discount > 0 ? true : false,
      child: Label(
        triangleHeight: 10.0,
        edge: Edge.RIGHT,
        child: Container(
          width: 89,
          height: 26,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              CommonColors.orangeF2,
              CommonColors.redFF,
            ],
          )),
          child: Text('${discount.toString()}% OFF',
              style: CommonStyles.w500Size12WhiteFF(context)),
        ),
      ),
    );
  }

  static displayCardVote(BuildContext context, String text) {
    return GestureDetector(
        onTap: () {},
        child: Container(
          padding: const EdgeInsets.only(left: 4, right: 4, top: 2, bottom: 2),
          color: CommonColors.orangeF2,
          child: Row(
            children: [
              Text(text, style: CommonStyles.w900Size14WhiteFF(context)),
              Padding(
                padding: const EdgeInsets.only(left: 2),
                child: SvgPicture.asset(Resources.icStartVote),
              )
            ],
          ),
        ));
  }
}
