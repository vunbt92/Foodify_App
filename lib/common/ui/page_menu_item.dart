import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';
import 'package:flutter_base/common/common_styles.dart';
import 'package:flutter_base/common/ui/cards.dart';
import 'package:flutter_base/models/food_and_drink.dart';

class PageMenuItem extends StatelessWidget {
  final Data item;

  const PageMenuItem(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 230,
        child: Column(
          children: [
            Divider(color: CommonColors.whiteFF, height: 0.5),
            Stack(
              children: [
                Image.asset(item.pathImage!),
                Padding(
                    padding: const EdgeInsets.only(top: 10, left: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Cards.displayCardTitle(context, item.name!),
                        const SizedBox(height: 5),
                        Visibility(
                            visible: item.disCount == 0 ? false : true,
                            child: Cards.displayCardDiscount(
                                context, item.disCount!))
                      ],
                    ))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(item.title!,
                    style: CommonStyles.w400Size18WhiteFF(context)),
                Cards.displayCardVote(context, item.totalVote.toString())
              ],
            ),
            const SizedBox(height: 5),
            Text(
              item.content!,
              style: CommonStyles.w400Size12WhiteFF(context),
            )
          ],
        ));
  }
}
