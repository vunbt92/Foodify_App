import 'package:flutter/material.dart';
import 'package:flutter_base/common/common_colors.dart';

class CommonStyles {
  static TextStyle w400Size12Black00(BuildContext context) {
    return TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.black00);
  }

  static TextStyle w400Size12WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size14WhiteEC(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteEC);
  }

  static TextStyle w400Size14WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size14Grey(BuildContext context) {
    return const TextStyle(
        fontSize: 14.0, fontWeight: FontWeight.w400, color: Colors.grey);
  }

  static TextStyle w400Size14RedFF(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.redFF,
        letterSpacing: 2);
  }

  static TextStyle w400Size14OrangeFC(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.orangeFC,
        letterSpacing: 2);
  }

  static TextStyle w400Size16OrangeF2(BuildContext context) {
    return TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.orangeF2);
  }

  static TextStyle w400Size16WhiteEC(BuildContext context) {
    return TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteEC);
  }
  static TextStyle w400Size16Black00(BuildContext context) {
    return TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.black00);
  }

  static TextStyle w400Size16WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size18WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size18Black00(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.black00);
  }

  static TextStyle w400Size24WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size26Black00(BuildContext context) {
    return TextStyle(
        fontSize: 26.0,
        fontWeight: FontWeight.w400,
        color: CommonColors.black00);
  }

  static TextStyle w500Size12OrangeF2(BuildContext context) {
    return TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.orangeF2);
  }

  static TextStyle w500Size12WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.whiteFF);
  }

  static TextStyle w500Size14WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.whiteFF);
  }

  static TextStyle w500Size14Black29(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.black29);
  }

  static TextStyle w500Size18WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.whiteFF);
  }

  static TextStyle w500Size22Black00(BuildContext context) {
    return TextStyle(
        fontSize: 22.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.black00);
  }

  static TextStyle w500Size24OrangeF2(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.orangeF2);
  }

  static TextStyle w500Size24WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w500,
        color: CommonColors.whiteFF);
  }

  static TextStyle w600Size18Black00(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w600,
        color: CommonColors.black00);
  }

  static TextStyle w700Size14WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.whiteFF);
  }

  static TextStyle w700Size14Black00(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.black00);
  }

  static TextStyle w700Size18WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.whiteFF);
  }

  static TextStyle w700Size18Green54(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.green54,
        letterSpacing: 2);
  }

  static TextStyle w700Size18RedFF(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.redFF,
        letterSpacing: 2);
  }

  static TextStyle w700Size18OrangeF2(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeF2);
  }

  static TextStyle w700Size18OrangeFC(BuildContext context) {
    return TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeFC,
        letterSpacing: 2);
  }

  static TextStyle w700Size20WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.whiteFF);
  }

  static TextStyle w700Size22Black00(BuildContext context) {
    return TextStyle(
        fontSize: 22.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.black00);
  }

  static TextStyle w700Size24WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.whiteFF);
  }

  static TextStyle w700Size22OrangeF2(BuildContext context) {
    return TextStyle(
        fontSize: 22.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeF2);
  }

  static TextStyle w700Size24OrangeF3(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeF3);
  }

  static TextStyle w700Size24OrangeFC(BuildContext context) {
    return TextStyle(
        fontSize: 24.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeFC);
  }

  static TextStyle w700Size32OrangeF3(BuildContext context) {
    return TextStyle(
        fontSize: 32.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.orangeF3);
  }

  static TextStyle w700Size36WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 36.0,
        fontWeight: FontWeight.w700,
        color: CommonColors.whiteFF);
  }

  static TextStyle w900Size14WhiteFF(BuildContext context) {
    return TextStyle(
        fontSize: 14.0,
        fontWeight: FontWeight.w900,
        color: CommonColors.whiteFF);
  }

  static TextStyle w400Size18Grey(BuildContext context) {
    return const TextStyle(
        fontSize: 18, fontWeight: FontWeight.w400, color: Colors.grey);
  }

  static TextStyle w400Size18Black(BuildContext context) {
    return const TextStyle(
        fontSize: 18, fontWeight: FontWeight.w400, color: Colors.black);
  }

  static TextStyle w700Size22Black(BuildContext context) {
    return const TextStyle(
        fontSize: 22, fontWeight: FontWeight.w700, color: Colors.black);
  }

  static TextStyle w400Size26Black(BuildContext context) {
    return const TextStyle(
        fontSize: 26, fontWeight: FontWeight.w400, color: Colors.black);
  }
}
