import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_base/blocs/base_bloc_custom.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_event.dart';
import 'package:flutter_base/blocs/bill_bloc/bill_state.dart';
import 'package:flutter_base/models/food_and_drink.dart';
import 'package:flutter_base/models/item_bill.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BillBloc extends BaseBlocCustom<BillEvent, BillState> {
  BillBloc() : super(const BillInitial()) {
    on<RequestAddItemBill>((event, emit) => _handleGetItemBill(event, emit));
    on<RequestGetItemFoodDrink>(
        (event, emit) => _handleGetFoodDrink(event, emit));
    // Search item
    on<RequestSearchItem>((event, emit) => _handleSearchItem(event, emit));
    on<RequestGetItemDiscount>(
        (event, emit) => _handleGetItemDiscount(event, emit));
  }

  _handleGetItemBill(RequestAddItemBill event, Emitter emit) async {
    emit(const BillLoading());
    try {
      ItemBill itemBill = event.itemBill;
      emit(ItemBillLoaded(itemBill));
    } catch (e) {
      emit(BillLoadError(message: handleError(e)));
    }
  }

  _handleGetFoodDrink(RequestGetItemFoodDrink event, Emitter emit) async {
    emit(const ItemFoodDrinkLoading());
    try {
      FoodAndDrink foodAndDrink = await readJsonFoodDrink();
      emit(ItemFoodDrinkLoaded(foodAndDrink));
    } catch (e) {
      emit(ItemFoodDrinkError(message: handleError(e)));
    }
  }

  _handleGetItemDiscount(RequestGetItemDiscount event, Emitter emit) async {
    emit(const ItemFoodDrinkLoading());
    try {
      List<Item> listItemDisCount = await readJsonItemDiscount();
      emit(ItemDiscountLoaded(listItemDisCount));
    } catch (e) {
      emit(ItemFoodDrinkError(message: handleError(e)));
    }
  }

  _handleSearchItem(RequestSearchItem event, Emitter emit) async {
    emit(const SearchLoading());
    try {
      emit(SearchLoaded(event.dataSearch));
    } catch (e) {
      emit(SearchLoadError(message: handleError(e)));
    }
  }

  // Fetch data from the json file
  Future<FoodAndDrink> readJsonFoodDrink() async {
    final String response =
        await rootBundle.loadString('assets/jsons/data_food_drink.json');
    final data = await json.decode(response);
    return FoodAndDrink.fromJson(data);
  }

  // Fetch data from the json file
  Future<List<Item>> readJsonItemDiscount() async {
    final String response =
        await rootBundle.loadString('assets/jsons/data_best_seller.json');
    Iterable list = jsonDecode(response);
    return list.map((e) => Item.fromJson(e)).toList();
  }
}
