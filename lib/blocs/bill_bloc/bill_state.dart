import 'package:flutter_base/models/item_bill.dart';
import 'package:flutter_base/models/food_and_drink.dart';

abstract class BillState {
  const BillState();
}

class BillInitial extends BillState {
  const BillInitial();
}

class BillLoading extends BillState {
  const BillLoading();
}

class ItemBillLoaded extends BillState {
  final ItemBill itemBill;

  const ItemBillLoaded(this.itemBill);
}

class BillLoadError extends BillState {
  final String message;

  const BillLoadError({required this.message});
}

class ItemFoodDrinkLoading extends BillState {
  const ItemFoodDrinkLoading();
}

class ItemFoodDrinkLoaded extends BillState {
  FoodAndDrink foodAndDrink;

  ItemFoodDrinkLoaded(this.foodAndDrink);
}

class ItemFoodDrinkError extends BillState {
  final String message;

  const ItemFoodDrinkError({required this.message});
}

class ItemDiscountLoading extends BillState {
  const ItemDiscountLoading();
}

class ItemDiscountLoaded extends BillState {
  List<Item> itemDiscount;

  ItemDiscountLoaded(this.itemDiscount);
}

class ItemDiscountError extends BillState {
  final String message;

  const ItemDiscountError({required this.message});
}

class SearchLoading extends BillState {
  const SearchLoading();
}

class SearchLoaded extends BillState {
  final String dataSearch;

  const SearchLoaded(this.dataSearch);
}

class SearchLoadError extends BillState {
  final String message;

  const SearchLoadError({required this.message});
}
