import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/item_bill.dart';

abstract class BillEvent extends Equatable {
  const BillEvent();

  @override
  List<Object?> get props => [];
}

class RequestGetItemFoodDrink extends BillEvent {
  const RequestGetItemFoodDrink();

  @override
  List<Object?> get props => [];
}

class RequestGetItemDiscount extends BillEvent {
  const RequestGetItemDiscount();

  @override
  List<Object?> get props => [];
}

class RequestAddItemBill extends BillEvent {
  final ItemBill itemBill;

  const RequestAddItemBill(this.itemBill);

  @override
  List<Object?> get props => [itemBill];
}

class RequestSearchItem extends BillEvent {
  final String dataSearch;

  const RequestSearchItem(this.dataSearch);

  @override
  List<Object?> get props => [dataSearch];
}
