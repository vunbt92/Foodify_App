import 'package:flutter_base/blocs/base_bloc_custom.dart';
import 'package:flutter_base/blocs/page/page_even.dart';
import 'package:flutter_base/blocs/page/page_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PageBloc extends BaseBlocCustom<PageEvent, PageState> {
  PageBloc() : super(const PageInitial()) {
    on<RequestChangePage>((event, emit) => _handleChangPage(event, emit));
  }

  _handleChangPage(RequestChangePage event, Emitter emit) async {
    emit(const PageLoading());
    try {
      emit(PageLoaded(event.moreItemModel));
    } catch (e) {
      emit(PageLoadError(message: handleError(e)));
    }
  }
}
