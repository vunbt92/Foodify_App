import 'package:flutter_base/models/more_item_model.dart';

abstract class PageState {
  const PageState();
}
class PageInitial extends PageState {
  const PageInitial();
}

class PageLoading extends PageState {
  const PageLoading();
}

class PageLoaded extends PageState {
  final MoreItemModel moreItemModel;

  const PageLoaded(this.moreItemModel);
}

class PageLoadError extends PageState {
  final String message;

  const PageLoadError({required this.message});
}
