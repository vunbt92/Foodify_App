import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/more_item_model.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();

  @override
  List<Object?> get props => [];
}

class RequestChangePage extends PageEvent {
  final MoreItemModel moreItemModel;

  const RequestChangePage(this.moreItemModel);

  @override
  List<Object?> get props => [moreItemModel];
}
